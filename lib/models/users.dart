import 'dart:convert';
import 'package:demo_app_bryk/utils/serviceReq.dart';

class UsersGithub {
  final String username;
  final String avatar;
  final String link;

  UsersGithub({this.username, this.avatar, this.link});

  factory UsersGithub.fromJson(Map<String, dynamic> json) {
    return UsersGithub(
        username: json['login'],
        avatar: json['avatar_url'],
        link: json['html_url']);
  }

  //Call API Trans Obj
  static ReService<List<UsersGithub>> get all {
    return ReService(
        url: 'https://api.github.com/users',
        parse: (response) {
          final result = json.decode(response.body);
          Iterable list = result;
          return list.map((model) => UsersGithub.fromJson(model)).toList();
        });
  }
}
