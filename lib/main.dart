import 'package:flutter/material.dart';

//Screens
import './screens/welcome.dart';
import './screens/about.dart';
import './screens/users.dart';

//Main Import App
void main() => runApp(MasterApp());

///
// Container App and routers.
// @class
///
class MasterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo App',
      initialRoute: '/',
      routes: {
        "/": (BuildContext context) => WelcomePage(),
        "/users": (BuildContext context) => UsersPage(),
        "/about": (BuildContext context) => AboutPage(),
      },
    );
  }
}
