import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class ReService<T> {
  final String url;
  T Function(Response response) parse;

  ReService({this.url, this.parse});
}

class ServiceReq {
  Future<T> load<T>(ReService<T> resource) async {
    final res = await http.get(resource.url);

    if (res.statusCode == 200) {
      return resource.parse(res);
    } else {
      throw Exception('Something happend!');
    }
  }
}
