import 'package:flutter/material.dart';

import 'package:flutter/widgets.dart';
import 'package:demo_app_bryk/models/users.dart';
import 'package:demo_app_bryk/utils/serviceReq.dart';

class UsersGithubState extends State<UsersPage> {
  List<UsersGithub> _usersGithub = List<UsersGithub>();

  @override
  void initState() {
    super.initState();
    _fetchUsersGithub();
  }

  //Req API
  void _fetchUsersGithub() {
    ServiceReq().load(UsersGithub.all).then((usersGithub) => {
          setState(() => {_usersGithub = usersGithub})
        });
  }

  //Build List View
  ListTile _buildItemsForListView(BuildContext context, int index) {
    return ListTile(
      title: Text(_usersGithub[index].username, style: TextStyle(fontSize: 18)),
      subtitle: Text(_usersGithub[index].link, style: TextStyle(fontSize: 18)),
      leading: Image.network(_usersGithub[index].avatar),
    );
  }

  //Build View (Page)
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Users Github'),
      ),
      body: ListView.builder(
        itemCount: _usersGithub.length,
        itemBuilder: _buildItemsForListView,
      ),
    );
  }
}

///
// Page (users) - Users from Github (Demo API).
// @class
///
///
class UsersPage extends StatefulWidget {
  @override
  createState() => UsersGithubState();
}
