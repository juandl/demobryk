import 'package:flutter/material.dart';

///
// Home Page (With Links)
// @class
///
class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text(
                'Users Github',
              ),
              onPressed: () {
                Navigator.pushNamed(context, "/users");
              },
            ),
            RaisedButton(
              child: Text(
                'About Us',
              ),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  "/about",
                  arguments: <String, String>{
                    'company':
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget auctor diam. Duis imperdiet tortor eu nisl consequat eleifend. Aliquam tellus erat, laoreet interdum lorem non, maximus bibendum mi. ',
                    'resource': 'This Text came from Argumnets',
                  },
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
