import 'package:flutter/material.dart';

///
// Page About Us.
// @class
///
class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map args = ModalRoute.of(context).settings.arguments as Map;
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
      ),
      body: Column(
        children: <Widget>[
          Text(args['company']),
          Text(args['resource']),
        ],
      ),
    );
  }
}
